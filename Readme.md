### Simple nginx-html container

To be recognize by reverse-proxy container, create a container like this:

```
version: '3.3'
services:
  proxy:
    container_name: app_nginx_8001
    image: nginx
    volumes:
      - .:/usr/share/nginx/html
    networks:
      - app_network
    ports:
      - 8001:80

networks:
  app_network:
    external:
      name: app_network
```
