start: ##@Docker start
	docker network create app_network || true
	docker-compose up -d

# kill: ##@Docker destroy all
# 	docker stop $(docker ps -a -q)
# 	docker rm $(docker ps -a -q)
# 	docker system prune -a
